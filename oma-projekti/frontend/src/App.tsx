import {
  BrowserRouter as Router,
  Routes, Route, Link
} from "react-router-dom";
import './App.css';
import Text from "./Text";
import Admin from "./Admin";
import Welcome from "./Welcome";

function App() {
  return (
    <Router>
      <div>
        <Link className="" to="/Text">Text </Link>
        <Link className="" to="/Admin">Admin</Link>
      </div>
      <Routes>
        <Route path="/" element={<Welcome />}></Route>
        <Route path="/Text" element={<Text />}></Route>
        <Route path="/Admin" element={<Admin />}></Route>
      </Routes>
    </Router>
  );
}

export default App;
