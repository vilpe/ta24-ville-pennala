import axios from 'axios';
import { useEffect, useState } from 'react';

const Admin = () => {
    const handlePost = (event: React.FormEvent<HTMLFormElement>) => {
        const formInputs = new FormData(event.currentTarget);
        console.log(formInputs.entries());
        /*axios.post('http://localhost:5000', {
            text_id: Number(formInputs.get('text_id')), //uuid
            name: formInputs.get('name')
        }) 
        .catch ((err: unknown) => {
console.log(err);
});
*/
    };
    const checkText = ((event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        const formInputs = new FormData(event.currentTarget);
        const text = formInputs.get('area') as string;
        const sentences = text.split('.')
            .map(sentence => {
                return sentence.replace('\n', '').trim();
            })
            .filter(sentence => sentence != '');
        const newData = sentences.map((sentence, index) => {
            return { id: index, sentence: sentence };
        });
        setFormData(newData);
    });

    const [formData, setFormData] = useState<{ id: number, sentence: string }[]>([]);
    const postSentences = ((event: React.FormEvent<HTMLFormElement>) => {
        console.log('here', formData);
        const formInputs = new FormData(event.currentTarget);
        //formi ei taida päivittyä
        const postable = formData.map(each => {
            return each.sentence;
        });
        axios.post('http://localhost:5000', postable)
            .then(res => { console.log(res); })
            .catch((err: unknown) => { console.log(err); });
        event.preventDefault();
    });

    const sentencesInParts = formData.map((data, index) => {
        return <input
            key={index}
            placeholder={data.sentence}
            value={data.sentence}
            onChange={(e) => {
                const newSentence = formData.map((data, i) => {
                    return index === i ? { ...data, sentence: e.target.value } : data;
                });
                setFormData(newSentence);
            }}
        ></input>;
    });
    const formStyle = {
        display: 'flex',
        flexDirection: 'column',
        width: '10em',
        border: '1px solid black'
    };
    const area = {
        width: '400px',
        height: '400px'
    };

    return (
        <div style={formStyle}>
            <form onSubmit={handlePost}>
                Post text
                <input placeholder='id in uuid form' name='text_id'></input>
                <input placeholder='name of text' name='name'></input>
                <button type='submit'>Delete</button>
            </form>
            <form onSubmit={checkText}>
                A bunch of text.
                <textarea style={area} name="area"></textarea>
                <button type='submit'>Split into sentences</button>
            </form>
            <form onSubmit={postSentences}>
                {sentencesInParts}
                <button type='submit'>Post sentences</button>
            </form>
        </div>
    );
};

export default Admin;