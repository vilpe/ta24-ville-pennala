import { useEffect } from "react";
import { generate } from 'random-words';


const Welcome = () => {
    useEffect(() => {
        const word = generate({ exactly: 1, wordsPerString: 3 });
        console.log(word);
    }, []);
    return (
        <div>
            welcome page
        </div>
    );
};
export default Welcome;