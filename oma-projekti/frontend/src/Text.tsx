import { useEffect, useState } from 'react';
import axios from 'axios';

const Text = () => {
    interface TextsModel {
        text_id: string,
        name: string
    }
    interface SentenceModel {
        text_id: string,
        sentence_id: number,
        sentence: string
    }
    interface CommentModel {
        sentence_id: number,
        comment_id: number,
        comment: string
    }
    const [loading, setLoading] = useState(false);
    const [texts, setTexts] = useState<TextsModel[]>([]);
    const [sentences, setSentences] = useState<SentenceModel[]>([]);
    const [comments, setComments] = useState<CommentModel[]>([]);

    useEffect(() => {
        setLoading(true);
        axios.get<TextsModel[]>('http://localhost:5000')
            //miks toimii? axios response palauttaa vaikka mitä
            .then((res) => {
                setTexts([...res.data]);
                console.log();
            })
            .catch((err: unknown) => {
                console.log(err);
            })
            .finally(() => {
                setLoading(false);
            });
    }, []);

    const textsComp = texts.map(text => {
        return <p key={text.text_id}>{text.name}<button onClick={() => { getSentences(text.text_id); }}>Get</button></p>;
    });
    const sentenceComp = sentences.map(sentence => {
        return <p key={sentence.sentence_id}>{sentence.sentence}<button onClick={() => { getComments(sentence.sentence_id); }}>Get Comments</button></p>;
    });
    const commentComp = comments.map(comment => {
        return <p key={comment.comment_id}>{comment.comment}</p>;
    });
    const getSentences = (id: string) => {
        axios.get<SentenceModel[]>(`http://localhost:5000/${id}`)
            .then(res => {
                setSentences([...res.data]);
            })
            .catch((err: unknown) => {
                console.log(err);
            });
    };
    const getComments = (id: number) => {
        axios.get<CommentModel[]>(`http://localhost:5000/comments/${String(id)}`)
            .then(res => {
                setComments([...res.data]);
            })
            .catch((err: unknown) => {
                console.log(err);
            });
    };

    return (
        <div className='base'>
            <div>
                {loading &&
                    <p>Loading data</p>}
                {textsComp}
                {sentenceComp}
            </div>
            <div>
                {comments.length > 0 ?
                    <p>{commentComp}</p> :
                    <p>no comments</p>}
            </div>
        </div>
    );
};

export default Text;