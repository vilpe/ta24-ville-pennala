import express from 'express';
import 'dotenv/config';
import { query } from './db.ts';
import cors from 'cors';
console.log(process.env.PORT);

const server = express();

server.use(express.json());
server.use(cors());

function checkUser(req: express.Request, res: express.Response, next: express.NextFunction) {
    if (req.path == '/') return next();

    //authenticate user
    next();
}
function logger(req: express.Request, res: express.Response, next: express.NextFunction) {
    //console.log(req.body);
    next();
}

server.all('*', logger, checkUser);
server.get('/', async (_req, res) => {
    const text = await query('SELECT * FROM texts');

    const text2 = await query('SELECT * FROM text_sentences WHERE text_id = $1', [text.rows[0].text_id]);

    const text3 = await query('SELECT * FROM sentence_comments WHERE sentence_id = $1', [text2.rows[0].sentence_id]);

    res.send(text.rows);
});
server.get('/:text_id', async (req, res) => {
    const text = await query('SELECT * FROM text_sentences WHERE text_id = $1', [req.params.text_id]);
    res.send(text.rows);
});
server.get('/comments/:sentence_id', async (req, res) => {
    const text = await query('SELECT * FROM sentence_comments WHERE sentence_id = $1', [req.params.sentence_id]);
    res.send(text.rows);
});
server.post('/', (req, res) => {
    console.log(req.body[0]);
    res.send('ok');
});

server.get('/example/b', (_req, res, next) => {
    console.log('the response will be sent by the next function ...');
    next();
}, (_req, res) => {
    res.send('Hello from B!');
});
server.route('/book')
    .get((req, res) => {
        res.send('Get a random book');
    })
    .post((req, res) => {
        res.send('Add a book');
    })
    .put((req, res) => {
        res.send('Update the book');
    });

server.listen(process.env.PORT, () => {
    console.log('listening on ' + process.env.PORT);
});


