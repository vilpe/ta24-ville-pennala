import pg from 'pg';
import 'dotenv/config';

const clientConfig = {
    host: 'localhost',
    password: 'password',
    user: 'ville',
    database: 'projektidb',
    port: 3000,
    allowExitOnIdle: true
};

const { Pool } = pg;

const pool = new Pool(clientConfig);


/*pool.on('error', (err) => {
    console.log({ err });
}); */
async function query(sql: string, values?: string[]) {
    const client = await pool.connect();
    try {
        const res = await client.query(sql, values);
        await client.release();
        return res;
    } catch (error) {
        await client.release(true);
        return error;
    }
}

export { query };







