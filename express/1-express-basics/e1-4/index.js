import express from 'express'

const server = express()

server.listen(3000, () => {
    console.log('Listening to port 3000')
})

// server.get("/", (req,res) => {
//     res.send("hello world")
// })
// server.get("/endpoint2", (req,res) => {
//     res.send({text:"hello"})
// })
// server.get("/reqtest/:name",(req,res) => {
//     res.send("hello " + req.params.name)
// })
let counterCount = 0;
let users = {};
server.get("/counter/:name", (req, res) => {
    if (req.query.number) {
        //http://localhost:3000/counter?number=10
        counterCount = req.query.number;
    } else {
        users[req.params.name] = (users[req.params.name] + 1 || 1);
        counterCount++;
        res.send(req.params.name + " was here " + users[req.params.name] + " times");

    }
    //res.send("times visited: "+counterCount)
})