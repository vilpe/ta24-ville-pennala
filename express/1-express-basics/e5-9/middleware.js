
export const logger = (req, res, next) => {
    console.log(new Date().toDateString())
    console.log(req.method)
    console.log(req.originalUrl)
    if (req.body) {console.log(req.body)};
    next();
}
export const unknownEndpoint = (_req, res) => {
    res.status(404).send({ error: 'no one here' })
}

