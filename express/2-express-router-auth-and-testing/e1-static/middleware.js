import jwt from 'jsonwebtoken';

export const logger = (req, res, next) => {
    console.log(new Date().toDateString())
    console.log(req.method)
    console.log(req.originalUrl)
    if (req.body) { console.log({body:req.body}) };
    next();
}
export const unknownEndpoint = (_req, res) => {
    res.status(404).send({ error: 'no one here' })
}

export const authenticate = (req, res, next) => { 
    console.log("trying to auth "+req.get('Authorization'))
    const auth = req.get('Authorization')
    if (!auth?.startsWith('Bearer ')) {
        return res.status(401).send('invalid token')
    }
    const token = auth.substring(7);
    try {
        const decoded = jwt.verify(token,process.env.SECRET)
        req.token = decoded;
        next();
    } catch (err) {
        console.log('bad token')
    }
}