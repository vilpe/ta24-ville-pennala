import request from 'supertest'
import server from './app.js'

describe('Server', () => {
    it('Returns 404 on invalid address', async () => {
        const response = await request(server)
            .get('/invalidaddress')
        expect(response.statusCode).toBe(404)
    })
    it('Returns 200 on valid address', async () => {
        const response = await request(server)
            .get('/')
        expect(response.statusCode).toBe(200)
        // expect(response.text).toEqual('Hello World!')
        //mulla index.html
    })
    it('Register new user', async () => {
        const res = await request(server)
            .post('/users/register')
            .send({username:"Ville",password:"salasana"})
            expect(res.statusCode).toBe(200)
            expect(res.text)
            //tsekkaa jwt?

    })
    it('Login new user', async () => {
        const res = await request(server)
            .post('/users/login')
            .send({username:"Ville",password:"salasana"})
            expect(res.statusCode).toBe(204)

    })
})
