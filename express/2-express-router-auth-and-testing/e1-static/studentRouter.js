import express from 'express';
import { logger, authenticate } from './middleware.js'

const router = express.Router();

router.use("/", logger)

let studentArray = [
    { id: 1, name: "ville", email: "dogmail.com" },
    { id: 2, name: "kalle", email: "mail.com" }
];

router.get("/", authenticate, (req, res) => {
    const idArray = studentArray.map(e => {
        return { id: e.id }
    })
    res.send(idArray)
})

router.post("/", authenticate, (req, res) => {
    if (!req.body.id || !req.body.name || !req.body.email) {
        res.status(400).send("Missing fields!")
    } else {
        studentArray.push({
            id: req.body.id,
            name: req.body.name,
            email: req.body.email
        })
        res.status(201).send(studentArray)
    }
})
router.get("/:id", (req, res) => {
    const student = studentArray.find(i => i.id === Number(req.params.id))
    if (student === undefined) {
        res.status(404).send("no such student")
    } else {
        res.send(JSON.stringify(student))
    }
})
router.put("/:id", (req, res) => {
    const student = studentArray.find(i => i.id === Number(req.params.id))
    if (student === undefined) {
        res.status(404).send('no student cannot put')
    } else {
        if (!req.body.email && !req.body.name) {
            res.status(400).send("missing fields!")
        } else {
            const index = studentArray.findIndex(i => i.id === Number(req.params.id))
            studentArray[index].email = req.body.email
            studentArray[index].name = req.body.name
            res.status(204)
        }
    }
})
router.delete("/:id", (req, res) => {
    const student = studentArray.find(i => i.id === Number(req.params.id))
    if (student === undefined) {
        res.status(404).send('no student cannot delete')
    } else {
        const index = studentArray.findIndex(i => i.id === Number(req.params.id))
        studentArray.splice(index, 1)
        res.status(204)
    }
})

export default router;