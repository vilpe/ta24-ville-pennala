import express from 'express';
import argon2, { verify } from 'argon2';
import 'dotenv/config';
import jwt from 'jsonwebtoken';

const router = express.Router();

let users = [];

async function makeAdminPass() {
    const pass = await argon2.hash('PASSWORD')
    console.log(pass)
}
// makeAdminPass();
async function printJWT() {
    const payload = { name: "ville" };
    const secret = process.env.SECRET;
    const options = { expiresIn: "15m" };
    const token = await jwt.sign(payload, secret, options);
    //console.log(token)
    return token
}
printJWT()
async function verifyJWT() {
    const token = await printJWT()
    let decoded = ""
    try {
        decoded = await jwt.verify(token, process.env.SECRET)
    } catch (err) {
        console.log(err)
    }
    // console.log(decoded)
}
verifyJWT()

router.post('/register', async (req, res) => {
    if (!req.body.username || !req.body.password) {
        res.status(400).send('Missing params!')
    } else {
        const password = await argon2.hash(req.body.password)
        users.push({
            username: req.body.username,
            password: password
        })
        //console.log(users)
        const payload = {username:req.body.username}
        const secret = process.env.SECRET;
        const options = {expiresIn:"1h"}
        const token = await jwt.sign(payload,secret,options);
        res.status(200).send(token);
    }
})
router.post('/login', async (req, res) => {
    const user = users.find(({ username }) => username === req.body.username);
    if (!user) {
        res.status(400).send('No User!')
    } else {
        const comparison = await argon2.verify(user.password, req.body.password)
        if (!comparison) {
            res.status(401).send('wrong pass');
        } else {
            console.log('successful login')
            res.status(204).send(); //jos ei send niin postman jää pyörii??
        }
    }
})

router.post('/admin', async (req, res) => {
    if (req.body.username === process.env.ADMIN_USERNAME) {
        console.log("right username")
        const comparison = await argon2.verify(process.env.ADMIN_HASH, req.body.password)
        if (!comparison) {
            res.status(401).send('wrong pass');
        } else {
            console.log("right pass")
            res.status(204).send()
        }
    }
})

export default router;