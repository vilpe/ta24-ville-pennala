import express from 'express'
import { unknownEndpoint } from './middleware.js'
import studentRouter from './studentRouter.js';
import userRouter from './userRouter.js';


const server = express()

server.use(express.json());
server.use("/students", studentRouter)
server.use("/users", userRouter)
server.use(express.static('public'));


server.use(unknownEndpoint)

export default server;