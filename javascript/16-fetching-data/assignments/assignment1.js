import axios from "axios";


async function getUniversities() {
	const res = await axios.get("http://universities.hipolabs.com/search?country=Finland");
	return res.data;
}
getUniversities()
	.then(res => {
		const names = res.map(e => {
			return e.name;
		});
		console.log(names);
	});