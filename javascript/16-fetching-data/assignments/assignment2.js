import axios from "axios";

async function getFakeStoreProducts() {
	const res = await axios.get("https://fakestoreapi.com/products");
	// console.log(res.data);
	const names = res.data.map(e => {
		return e.title;
	});
	console.log(names);
	return names;
}
//getFakeStoreProducts();
// console.log(getFakeStoreProducts());

async function addFakeStoreProduct(name,price,description,category) {
	axios.post("https://fakestoreapi.com/products", {
		title:name,
		price:price,
		description:description,
		image:"",
		category:category
	})
		.then(res => console.log(res.data.id))
		.catch(err => console.log(err));
}
addFakeStoreProduct("Lapanen",15,"Punanen","electronic");

async function deleteFakeStoreProduct(id) {
	axios.delete(`https://fakestoreapi.com/products/${id}`)
		.then(res => console.log(res.status));
}
deleteFakeStoreProduct(1);