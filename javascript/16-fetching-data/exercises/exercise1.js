import axios from "axios";
async function getPostById(id) {
	axios.get(`https://jsonplaceholder.typicode.com/posts/${id}`)
		.then(res1 => {
			axios.get(`https://jsonplaceholder.typicode.com/users/${res1.data.id}`)
				.then(res2 => {
					console.log(`Post #${res1.data.id} by ${res2.data.name}: ${res1.data.title}`);
				});        
		});
}
getPostById(1);

async function getAwaitPostById(id) {
	const res1 = await axios.get(`https://jsonplaceholder.typicode.com/posts/${id}`);
	const res2 = await axios.get(`https://jsonplaceholder.typicode.com/users/${res1.data.id}`);
	console.log(`Post #${res1.data.id} by ${res2.data.name}: ${res1.data.title}`);
}
getAwaitPostById(1);