import readline from "readline-sync";

let bot = "Botti"
let askedQuestions = 0;

const greeting = readline.question("What is your name? ")
console.log("Hello there, " +greeting)
while (true) {
    let input = readline.question(""); 
    if(input === "quit") {
        break;
    } else if (input==="help") {
        console.log(`
        -------------------------------
        Here's a list of commands that i can execute!

        help: opens this dialog
        hello: i will say hello to you
        botInfo: i will introduce myself
        botName: i will tell my name
        botRename: you can rename me
        forecast: i will forecast tomorrows weather 100% accurate
        quit: quits the program
        `)
    } else if (input==="botName") {
        askedQuestions++;
        console.log("My name is currently "+ bot);
        console.log("If you want to change it, type botRename")
    } else if (input==="botRename") {
        let rename = readline.question("Change bots name to: ")
        let confirmation = readline.question("Are you happy with name: "+rename +" ")
        if(confirmation.toLowerCase()==="yes") {
            bot = rename;
        } else {
            console.log("Name not changed. My name is "+ bot)
        }
    } else if (input==="botInfo") {
        askedQuestions++;
        console.log("I am a dumb bot. You can ask me anything :). You have alredy asked me " + askedQuestions +" questions.")
    } else if (input==="forecast") {
        const cloudy = Math.random() >= 0.5 ? "yes": "no";
        const sunny = Math.random() >= 0.5 ? "yes": "no";
        const wind = Math.random() >= 0.5 ? "yes": "no";
        console.log(`
        Tomorrows weather will be...
        temperature ${Math.round(Math.random()*10)} celsius
        Cloudy: ${cloudy}
        Sunny: ${sunny}
        Windy: ${wind}
        `)
    }
}