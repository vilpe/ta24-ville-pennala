import fs from "fs";
const forecast =  {
    day: "monday",
    temperature: 20,
    cloudy: true,
    sunny: false,
    windy: false,
};
const data = JSON.stringify(forecast,null,4)
fs.writeFileSync("forecast_data.json",data,"utf8")

const incData = fs.readFileSync("forecast_data.json","utf8")
const outData = JSON.parse(incData)
// console.log(outData)
outData.temperature = 30;
// console.log(outData)

const dataChanged = JSON.stringify(outData,null,4)
fs.writeFileSync("forecast_data.json",dataChanged,"utf8")