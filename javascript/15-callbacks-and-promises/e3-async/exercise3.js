const getValue = function () {
	return new Promise((res) => {
		setTimeout(() => {
			res({ value: Math.random() });
		}, Math.random() * 1500);
	});
};
async function waiter() {
	const a = await getValue();
	const b = await getValue();
	console.log(`value 1 is ${a.value} and value 2 is ${b.value}`);
}
waiter();
getValue().then(val => {
	let a = val;
	getValue().then(val => {
		console.log(`value 1 is ${a.value} and value 2 is ${val.value}`);
	});
	//promise on ärsyttävä 
});