function countdown(time) {
	return new Promise(() => {
		let seconds = time;

		function count() {
			if (seconds>0) {
				console.log(seconds);
				seconds--;
				setTimeout(count,1000);
			} else {
				console.log("go");
			}
		}
		count();
	});
}
countdown(5);