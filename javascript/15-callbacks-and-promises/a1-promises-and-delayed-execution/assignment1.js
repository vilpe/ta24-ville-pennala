function sum(limit) {
	let sum = 0;
	for (let i=0;i<=limit;i++) {
		sum += i;
	}
	return sum;
}
console.log(sum(10));

const pro = new Promise(res => {
	setTimeout(() => {
		res(sum(50000));
	}, 2000);
});
pro.then(res => console.log(res));

function createDelayedCalculation(limit,milliseconds) {
	return new Promise(res => {
		setTimeout(() => {
			res(sum(limit));
		}, milliseconds); 
	});
}
// Prints 200000010000000 after a delay of 2 seconds
createDelayedCalculation(20000000, 2000).then(result => console.log(result));
// Prints 1250025000 after a delay of 0.5 seconds
createDelayedCalculation(50000, 500).then(result => console.log(result));
//Alempi suorittuu ekana koska promise on asyncroninen ja koodi jatkuu