function func(str: string) {
	const arr = str.split(" ");
	return {
        length: str.length,
        words: arr.length
    }
}
console.log(func("pitkä sana"));

const grades: Array<number|string> = [1,"2",3,4,5]
grades.forEach(e => {
    if (typeof e === "number" && e > 3) {
        console.log(e)
    }
})