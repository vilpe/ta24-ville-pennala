function buildUserInterface() {
    const mainWindow = { name: "MainWindow", width: 600, height: 400, children: [ ] };
    const buttonExit = { name: "ButtonExit", width: 100, height: 30, children: [ ] };
    mainWindow.children.push(buttonExit);

    const settingsWindow = { name: "SettingsWindow", width: 400, height: 300, children: [ ] };
    const buttonReturnToMenu = { name: "ButtonReturnToMenu", width: 100, height: 30, children: [ ] };
    settingsWindow.children.push(buttonReturnToMenu);
    mainWindow.children.push(settingsWindow);

    const profileWindow = { name: "ProfileWindow", width: 500, height: 400, children: [ ] };
    const profileInfoPanel = { name: "ProfileInfoPanel", width: 200, height: 200, children: [ ] };
    profileWindow.children.push(profileInfoPanel);
    mainWindow.children.push(profileWindow);

    return mainWindow;
}
function findControl(control,name) {
    // console.log("nimi "+control.name)
    // console.log(control.children[1].children[0].name)
    if (control.name === name) {
        return control;
    }

    if(control.children.length>0) {
        for (let i=0;i<control.children.length;i++) {
            const child = findControl(control.children[i],name)
            if (child != null) {
                return child
            }
        }
    } else {
        return null 
    }
}

const userInterfaceTree = buildUserInterface();
const find =  findControl(userInterfaceTree,"ProfileInfoPanel")
console.log("lopputulos")
console.log(find)
find.width += 100
console.log(find)
// console.log(userInterfaceTree)