const arr = [ "horse", "cow", "dog", "hamster", "donkey", "cat", "parrot" ]

for (animal of arr) {
    if(animal.toLowerCase().includes("e")) {
        console.log(animal)
    }
}
arr.forEach(animal => {
    if(animal.toLowerCase().includes("e")) {
        console.log(animal)
    }
})