const numbers = [4,7,1,8,5]

function incrementAll(arr) {
    const incremented = arr.map(e => e+1)
    return incremented
}
function decrementAll(arr) {
    const decremented = arr.map(e => e-1)
    return decremented
}
console.log(incrementAll(numbers))
console.log(decrementAll(numbers))