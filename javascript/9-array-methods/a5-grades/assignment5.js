const students = [ { name: "Sami", score: 24.75 },
                   { name: "Heidi", score: 20.25 },
                   { name: "Jyrki", score: 27.5 },
                   { name: "Helinä", score: 26.0 },
                   { name: "Maria", score: 17.0 },
                   { name: "Yrjö", score: 14.5  } ];
function getGrades(arr) {
    const graded = arr.map(e => {
        if(e.score<14) {
            return {name:e.name, grade:0}
        } else if(e.score>14&&e.score<=17) {
            return {name:e.name, grade:1}
        } else if(e.score>17&&e.score<=20) {
            return {name:e.name, grade:2}
        } else if(e.score>20&&e.score<=23) {
            return {name:e.name, grade:3}
        } else if(e.score>23&&e.score<=26) {
            //return "grade 4"
            return {name:e.name, grade:4}
        } else if(e.score>26) {
            return {name:e.name, grade:5}
        }
    })
    return graded
}
console.log(getGrades(students))