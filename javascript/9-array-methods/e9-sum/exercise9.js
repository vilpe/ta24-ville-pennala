const arr = [1,2,3,4,5,6,7,NaN]
let sum = 0;
for (let i=0;i<arr.length;i++) {
    if (!isNaN(arr[i])) {
        sum += arr[i]
    }
}
console.log(sum)
const sumRed = arr.reduce((a,c) => {
    if(typeof(c)==='number' && !isNaN(c)) {
        return a+c
    } else {
        return a
    }
},0)
console.log(sumRed)