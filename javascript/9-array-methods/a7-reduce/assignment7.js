const numbers = [1,2,3,4,5,6,7];
const voters = [
    {name:'Bob' , age: 30, voted: true},
    {name:'Jake' , age: 32, voted: true},
    {name:'Kate' , age: 25, voted: false},
    {name:'Sam' , age: 20, voted: false},
    {name:'Phil' , age: 21, voted: true},
    {name:'Ed' , age:55, voted:true},
    {name:'Tami' , age: 54, voted:true},
    {name: 'Mary', age: 31, voted: false},
    {name: 'Becky', age: 43, voted: false},
    {name: 'Joey', age: 41, voted: true},
    {name: 'Jeff', age: 30, voted: true},
    {name: 'Zack', age: 19, voted: false}
];
const wishlist = [
    { title: "Tesla Model S", price: 90000 },
    { title: "4 carat diamond ring", price: 45000 },
    { title: "Fancy hacky Sack", price: 5 },
    { title: "Gold fidgit spinner", price: 2000 },
    { title: "A second Tesla Model S", price: 90000 }
];
const arrays = [
    ["1", "2", "3"],
    [true],
    [4, 5, 6]
];

const sum = numbers.reduce((a,c) => a+=c,0)
const stringSum = numbers.reduce((a,c) => a+=c,"")
const voted = voters.reduce((a,c) => c.voted ? a+1 : a+0,0)
const buySum = wishlist.reduce((a,c) => a+c.price,0)
const arrayConcat = arrays.reduce((a,c) => a.concat(c))
function voterResults () {
    let results = {
        numYoungVotes: 0,
        numYoungPeople: 0,
        numMidVotesPeople: 0,
        numMidsPeople: 0,
        numOldVotesPeople: 0,
        numOldsPeople: 0
    }
    voters.reduce((a,c) => {
        if (c.age <=25 ) {
            results.numYoungPeople++
            if(c.voted===true) {
                results.numYoungVotes++
            }
        } else if (c.age>=26&&c.age<=35) {
            results.numMidsPeople++
            if(c.voted===true) {
                results.numMidVotesPeople++
            }
        } else if (c.age>=36&&c.age<=55) {
            results.numOldsPeople++ 
            if (c.voted===true) {
                results.numOldVotesPeople++
            }
        }
    },0)
    console.log(results)

}
voterResults()
// console.log(arrayConcat)