function exponentValueList(n,p) {
    let incr = 2;
    if (typeof(n) != "number" || n < 1) {
        console.log("n needs to be positive")
    } else {
        for (let i=0;i<n;i++) {
            console.log(incr)
            incr *= p;
        }
    }
}
exponentValueList(4,3)