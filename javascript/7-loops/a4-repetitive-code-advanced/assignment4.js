let vowels = [
    {vowel: "a", count: 0},
    {vowel: "e", count: 0},
    {vowel: "i", count: 0},
    {vowel: "o", count: 0},
    {vowel: "u", count: 0},
    {vowel: "y", count: 0}
];

function checkSentenceVowels(sentence) {

    for (let i=0;i<sentence.length;i++) {
        for(let j=0;j<vowels.length;j++) {
            if (sentence.charAt(i).toLowerCase() === vowels[j].vowel) {
                vowels[j].count += 1;
            } 
        }
    }

    console.log("A letter count: " + vowels[0].count);
    console.log("E letter count: " + vowels[1].count);
    console.log("I letter count: " + vowels[2].count);
    console.log("O letter count: " + vowels[3].count);
    console.log("U letter count: " + vowels[4].count);
    console.log("Y letter count: " + vowels[5].count);

    const totalCount = vowels[0].count + vowels[1].count
    + vowels[2].count + vowels[3].count + vowels[4].count + vowels[5].count

    console.log("Total vowel count: " + totalCount);
}

checkSentenceVowels("A wizard's job is to vex chumps quickly in fog.");
