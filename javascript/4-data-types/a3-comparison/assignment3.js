const person1Age = 15;
const person2Age = 24;

const isFirstPersonOlder = person1Age > person2Age;
console.log(isFirstPersonOlder); //false
console.log(typeof(isFirstPersonOlder))

const class1 = [9,6,9];
const class2 = [7,10,5];
let sum1 = 0;
let sum2 = 0; 
class1.forEach(e => {sum1 += e});
class2.forEach(e => {sum2 += e});
sum1 /= class1.length;
sum2 /= class2.length;
console.log(sum1)
console.log(sum2)
console.log((class1 > class2) ? true : false)
