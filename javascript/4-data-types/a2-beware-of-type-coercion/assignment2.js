//A
let number; //undefined
const result1 = 10 + number; //NaN

number = null;
const result2 = 10 + number;
//console.log(result2)
//JS hajoaa jos yrittää plussata undefinedin mutta NULL se ei yritä jolloin se toimii

//B
const a = true;
const b = false;

const c = a + b;
const d = 10 + a;
const e = 10 + b;
//true == 1, false == 0