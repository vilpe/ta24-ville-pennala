/*
const book1 = {
    name: "Dune",
    pageCount: 412,
    read: true
};
const book2 = {
    name: "The Eye of the World",
    pageCount: 782,
    read: false
}; 
book1.read = false;
book2.read = true;

console.log(book1)
console.log(book2) */
const books = [
    {
        name: "Dune",
        pageCount : 412,
        read: true
    },
    {
        name: "The Eye of the World",
        pageCount: 782,
        read: false
    }
];
books[0].read = false;
console.log(books)