const a = true;
const b = false;
if (a && b) {
    console.log("Both are true")
};
if (a && !b) {
    console.log("first true second false")
};
if (!a && b) {
    console.log("first is false second is true")
};
if (!a && !b) {
    console.log("both are false")
};