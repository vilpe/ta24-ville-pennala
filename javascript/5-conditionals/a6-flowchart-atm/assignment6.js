const balance = 2;
const isActive = true;
const checkBalance = false;

if (checkBalance) {
    if (isActive && balance > 0) {
        console.log("Your balance: " + balance)
    } else if (!isActive) {
        console.log("Your account is not active")
    } else if (isActive && balance === 0) {
        console.log("Your account is empty")
    } else {
        console.log("Your balance is negative")
    }
} else {
    console.log("Have a nice day!")
}