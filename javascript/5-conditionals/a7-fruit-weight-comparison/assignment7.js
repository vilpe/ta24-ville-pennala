const fruit1 = {
    name: "pear",
    weight: 178
};
const fruit2 = {
    name: "lemon",
    weight: 120
};
const fruit3 = {
    name: "apple",
    weight: 90
};
const fruit4 = {
    name: "mango",
    weight: 150
};

const averageWeight = (fruit1.weight+fruit2.weight+fruit3.weight+fruit4.weight)/4

const fruit1Diff = Math.abs(averageWeight-fruit1.weight)
const fruit2Diff = Math.abs(averageWeight-fruit2.weight)
const fruit3Diff = Math.abs(averageWeight-fruit3.weight)
const fruit4Diff = Math.abs(averageWeight-fruit4.weight)
console.log(fruit1Diff)
console.log(fruit2Diff)
console.log(fruit3Diff)
console.log(fruit4Diff)

if (
    fruit1Diff < fruit2Diff &&
    fruit1Diff < fruit3Diff &&
    fruit1Diff < fruit4Diff 
    ) {
        console.log(fruit1.name)
    } else if (
    fruit2Diff < fruit1Diff &&
    fruit2Diff < fruit3Diff &&
    fruit2Diff < fruit4Diff
    ) {
        console.log(fruit2.name)
    } else if (
    fruit3Diff < fruit1Diff &&
    fruit3Diff < fruit2Diff &&
    fruit3Diff < fruit4Diff
    ) {
        console.log(fruit3.name)
    } else if (
    fruit4Diff < fruit1Diff &&
    fruit4Diff < fruit2Diff &&
    fruit4Diff < fruit3Diff
    ) {
        console.log(fruit4.name)
    }
