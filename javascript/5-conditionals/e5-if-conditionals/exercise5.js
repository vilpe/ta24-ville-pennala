//A
if (playercount === 4) {
    playHearts();
}
//B
if (!isStressed || hasIceCream) {
    mark.happy = true;
}
//C
if (sunIsShining && !isRaining && temperature >= 20) {
    beachDay = true;
}
//D
if ((seesSuzy || seesDan) && day === "Tuesday") {
    arin.happy = true;
} else {
    arin.happy = false;
}