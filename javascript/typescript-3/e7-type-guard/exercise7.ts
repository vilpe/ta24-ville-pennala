
const typeGuard = (arr: any): arr is number[] => {
    for (let i = 0; i < arr.length; i++) {
        if (typeof arr[i] != 'number')
            return false;
    }
    return true;
}

const arr1 = [1, 2, 3, "b"];
const arr2 = [1, 2, 3, 4, 5, 6];
console.log(typeGuard(arr2))