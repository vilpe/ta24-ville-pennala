import fs from 'fs';

interface book {
    author: string,
    title: string,
    readingStatus: boolean,
    id: number
}

const library = [
    {
        author: 'David Wallace',
        title: 'Infinite Jest',
        readingStatus: false,
        id: 1,
    },
    {
        author: 'Douglas Hofstadter',
        title: 'Gödel, Escher, Bach',
        readingStatus: true,
        id: 2,
    },
    {
        author: 'Harper Lee',
        title: 'To Kill A Mockingbird',
        readingStatus: false,
        id: 3,
    },
];

function saveToJSON(file: book[]): void {
    fs.writeFileSync("library.json", JSON.stringify(file), "utf8")
}
saveToJSON(library);

function loadFromJSON() {
    const lib = fs.readFileSync("library.json", "utf8")
    return JSON.parse(lib)
}
const libArray = loadFromJSON()

function getBook(paramId: number) {
    const book = libArray.find(({ id }: { id: number }) => id === paramId)
    return book;
}
// console.log(getBook(1));

function printBookData(paramId: number) {
    const book = libArray.find(({ id }: { id: number }) => id === paramId)
    console.log(`${book.author} ${book.title}`)
}
// printBookData(1);

function addNewBook(author: string, title: string) {
    library.push({
        author: author,
        title: title,
        readingStatus: false,
        id: 4
    })
    console.log(library)
}
// addNewBook("Frank","Dune");

function readBook(pId: number) {
    const index = library.findIndex(({id}:{id:Number})=>id===pId)
    library[index].readingStatus = true
    console.log(library[index])
}
readBook(1)