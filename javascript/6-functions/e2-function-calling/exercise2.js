function sum(p1, p2) {
    return p1 + p2;
};

const sum1 = function(p1, p2) {
    return p1 + p2;
};

const sum2 = (p1,p2) => {return p1 + p2};

console.log(sum(1,2))
console.log(sum1(4,5))
console.log(sum2(10,20))