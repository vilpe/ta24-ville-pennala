const language = "ru"

function hello(lang) {
    switch (lang) {
        case "en":
            console.log("hello world");
            break;
        case "fi":
            console.log("Hei maailma!")
            break;
        case "ru":
            console.log("привет мир")
            break;
        default:
            console.log("no language specified!")
    }
}
hello("en")
hello("fi")
hello("ru")