const firstTriangle = { width: 7.0, length: 3.5 };
const secondTriangle = { width: 4.3, length: 6.4 };
const thirdTriangle = { width: 5.5, length: 5.0 };

function CalcTriangleArea(triangle) {
    return (triangle.width*triangle.length/2)
}
function BiggestTriangleArea() {
    if (CalcTriangleArea(firstTriangle) > CalcTriangleArea(secondTriangle) && CalcTriangleArea(firstTriangle) > CalcTriangleArea(thirdTriangle)) {
        console.log("First triangle has biggest area")
    } else if (CalcTriangleArea(secondTriangle) > CalcTriangleArea(firstTriangle) && CalcTriangleArea(secondTriangle) > CalcTriangleArea(thirdTriangle)) {
        console.log("Second triangle has biggest area")
    } else if (CalcTriangleArea(thirdTriangle) > CalcTriangleArea(secondTriangle) && CalcTriangleArea(thirdTriangle) > CalcTriangleArea(firstTriangle)) {
        console.log("Third triangle has biggest area")
    }
}
// console.log(CalcTriangleArea(firstTriangle))
/*
let firstTriangleArea = firstTriangle.width * firstTriangle.length;
firstTriangleArea = firstTriangleArea / 2.0;

let secondTriangleArea = secondTriangle.width * secondTriangle.length;
secondTriangleArea = secondTriangleArea / 2.0;

let thirdTriangleArea = thirdTriangle.width * thirdTriangle.length;
thirdTriangleArea = thirdTriangleArea / 2.0;
*/

console.log("Area of first triangle: " + CalcTriangleArea(firstTriangle));
console.log("Area of second triangle: " + CalcTriangleArea(secondTriangle));
console.log("Area of third triangle: " + CalcTriangleArea(thirdTriangle));
BiggestTriangleArea();