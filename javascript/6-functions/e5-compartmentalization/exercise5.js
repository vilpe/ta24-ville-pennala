const tree = { x: 6, y: 7, hitpoints: 30 };
const rock = { x: 3, y : 11, hitpoints: 90 };
const damage = 15;

function DamageTree(damageAmount) {
    tree.hitpoints -= damageAmount
    return tree.hitpoints
}
function DamageRock(damageAmount) {
    rock.hitpoints -= damageAmount
    return rock.hitpoints
}
function Damage(target, damageAmount) {
    target.hitpoints -= damageAmount
    return target.hitpoints
}


{

    console.log("Rock hitpoints left: " + DamageRock(damage));

    {

        console.log("Tree hitpoints left: " + Damage(tree,damage));
    }
}
