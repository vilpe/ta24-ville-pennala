const pc = { 
    name: "Buutti SuperCalculator 6000",
    cache: "96 GB",
    clockSpeed: 9001.0,
    overclock: function() {this.clockSpeed += 500},
    savePower: function() {
        if (this.clockSpeed>2000) {
            this.clockSpeed = 2000;
        } else {
            this.clockSpeed /= 2
        }
    },  
}
pc.overclock()
console.log(pc)
pc.savePower()
console.log(pc)
pc.savePower()
console.log(pc)