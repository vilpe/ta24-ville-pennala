const student = {
    name: "Ville",
    credits : 180,
    courseGrades : {
        "JS": 0,
        "mySQL": 4,
        "Recursion": 1
    }
}
console.log(student)
student.courseGrades.scrum = 5;
console.log(student)
student.courseGrades.JS = 4;
console.log(student)