class Room {
    constructor(width,height) {
        this.width = width
        this.height = height
        this.furniture = []
    }
    getArea() {
        return this.width*this.height
    }
    addFurniture(input) {
        this.furniture.push(input)
    }
}
const Olkkari = new Room(1.2,2,3)
Olkkari.addFurniture("sofa")
console.log(Olkkari)