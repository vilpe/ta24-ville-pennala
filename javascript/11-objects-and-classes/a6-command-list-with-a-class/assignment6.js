const commandList = "NNEESSWWCNNEEENNNCEESSSWNNNECEESWWNNNEEEBENNNEEE";
let x = 0;
let y = 0;
const arrayFuncs = [
   function () {y++},
   function () {x++},
   function () {y--},
   function () {x--},
   function () {}, 
]
const commandHandlers = {
    N: () => this.y++,
    E: () => this.x++,
    S: () => this.y--,
    W: () => this.x--,
    C: () => {},
}

class Robot {
    constructor(x,y) {
        this.x = x
        this.y = y
    }
    handleCommandList(list) {
        const commandHandlers = {
            N: () => this.y++,
            E: () => this.x++,
            S: () => this.y--,
            W: () => this.x--,
            C: () => {},
        }
        for (let i=0;i<list.length;i++) {
            if (list[i]==="N") {
                commandHandlers.N()
            } else if (list[i]==="E") {
                commandHandlers.E()
            } else if (list[i]==="S") {
                commandHandlers.S()
            } else if (list[i]==="W") {
                commandHandlers.W()
            } else if (list[i]==="C") {
                commandHandlers.C()
            } else if (list[i]==="B") {
                break;
            }
        }
    }
}

function commandIntoNumber(list) {
    for (let i=0;i<list.length;i++) {
        if (list[i]==="N") {
            commandHandlers.N()
        } else if (list[i]==="E") {
            commandHandlers.E()
        } else if (list[i]==="S") {
            commandHandlers.S()
        } else if (list[i]==="W") {
            commandHandlers.W()
        } else if (list[i]==="C") {
            commandHandlers.C()
        } else if (list[i]==="B") {
            break;
        }
    }
    //console.log(numbers)
}
//commandIntoNumber(commandList)
commandHandlers["N"]() //toimii mut loop???
const Rob = new Robot(0,0)
Rob.handleCommandList(commandList)
console.log(Rob)