class Animal {
    constructor(weight,cuteness) {
        this.weight = weight
        this.cuteness = cuteness
    }
    makeSound(){
        console.log("silence")
    }
}
class Cat extends Animal {
    constructor(weight,cuteness) {
        super(weight,cuteness)
    }
    makeSound(){
        console.log("meow")
    }
}
class Dog extends Animal {
    constructor(weight,cuteness,breed) {
        super(weight,cuteness)
        this.breed = breed
    }
    makeSound() {
        if (this.cuteness>4) console.log("awoo")
        else console.log("bark")
    }

}

const horse = new Animal(5,10)
horse.makeSound()
const cat = new Cat(2,9)
console.log(cat)
cat.makeSound()
const dog1 = new Dog(2,3,"hassu")
const dog2 = new Dog(3,9,'iso')
dog1.makeSound()
dog2.makeSound()