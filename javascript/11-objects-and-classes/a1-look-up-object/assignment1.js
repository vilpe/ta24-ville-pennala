const grades ="SABC"
function calculateTotalScore(grades) {
    const lookup = {
        "S":8,
        "A":6,
        "B":4,
        "C":3,
        "D":2,
        "F":0   
    }
    gradesSplit = grades.split("")
    const numbers = gradesSplit.map(e => {
        return e=lookup[e]
    });
    return numbers.reduce((a,c)=>a+c,0)
}
function calculateAverageScore(grades) {
    return calculateTotalScore(grades)/grades.length
}
// console.log(calculateTotalScore(grades))
console.log(calculateAverageScore(grades))
const arr = [ "AABAACAA", "FFDFDCCDCB", "ACBSABA", "CCDFABABC" ]
const p = arr.map(e => {
    return calculateAverageScore(e)
})
console.log(p)
