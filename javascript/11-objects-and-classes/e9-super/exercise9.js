class Robot {
    constructor(x,y) {
        this.x=x
        this.y=y
    }
    handleMessage(message) {
        if(message==="N") {this.y++}
        else if (message==="S") {this.y--}
        else if (message==="E") {this.x++}
        else if (message==="W") {this.x--}
    }
}
class FlexibleRobot extends Robot {
    handleMessage(message) {
        if (message==="NE") {this.x++;this.y++}
        else if (message==="NW") {this.x--;this.y++}
        else if (message==="SE") {this.x++;this.y--}
        else if (message==="SW") {this.x--;this.y--}
        else {super.handleMessage(message)}
    }
}
const a = new FlexibleRobot(0,0)
a.handleMessage("NE")
a.handleMessage("NE")
a.handleMessage("E")
a.handleMessage("E")
console.log(a)