const object = {
    hello: "hei",
    world: "maailma",
    bit: "bitti",
    byte: "tavu",
    integer: "kokonaisluku",
    boolean: "totuusarvo",
    string: "merkkijono",
    network: "verkko"
}

function printTranslatableWords(obj) {
    return Object.keys(obj);
}
function translate(str) {
    if(str in object) {
        return object[str]
    } else {
        console.log("no translation exists for the word "+str)
        return null
    }
}
console.log(printTranslatableWords(object))
console.log(translate("network"))