class Shape{
    constructor(width,height) {
        this.width = width;
        this.height = height;
    }
    getArea() {
        return 0;
    }
}
class Rectangle extends Shape {
    getArea() {
        return this.width*this.height
    }
}
class Ellipse extends Shape {
    getArea() {
        return Math.PI*(this.width/2)*(this.height/2)
    }
}
class Triangle extends Shape {
    getArea() {
        return (this.height*this.width)/2
    }
}
const a = new Rectangle(4,5)
const b = new Ellipse(10,20)
const c = new Triangle(9,10)
console.log(c.getArea())