class Shape{
    constructor(width,height) {
        this.width = width;
        this.height = height;
    }
    getArea() {
        return 0;
    }
}
class Rectangle extends Shape {
    getArea() {
        return this.width*this.height
    }
}
class Ellipse extends Shape {
    getArea() {
        return Math.PI*(this.width/2)*(this.height/2)
    }
}
class Triangle extends Shape {
    getArea() {
        return (this.height*this.width)/2
    }
}
class Square extends Rectangle {
    constructor(width) {
        super(width)
        this.height=width
    }
    getArea() {
        return this.height*this.width
    }
}
class Circle extends Ellipse {
    constructor(width) {
        super(width)
        this.height=width
    }
    getArea() {
        return Math.PI*this.width
    }
}
const a = new Rectangle(4,5)
const b = new Ellipse(10,20)
const c = new Triangle(9,10)
const d = new Square(10)
const e = new Circle(50)
console.log(e.getArea())