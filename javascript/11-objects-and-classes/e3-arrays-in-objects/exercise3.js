const student = {
    name: "Aili",
    credits: 45,

    courseGrades : [
        {name:"Intro to Programming",grade : 4},
        {name:"JavaScript Basics",grade: 3},
        {name:"Functional Programming",grade: 5}
    ]
}
const a = student.courseGrades.find(e => e.name==="JavaScript Basics")
console.log(`Aili got ${a.grade} from ${a.name}`)

function addCourse(courseName, courseGrade) {
    student.courseGrades.push({name:courseName,grade:courseGrade})
    
}
console.log(student)
addCourse("baking",5)
console.log(student)