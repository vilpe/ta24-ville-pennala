const fruits = {
    banana:118,
    apple:85,
    mango:200,
    lemon:65
}

function printWeight(fruit) {
    if (fruit in fruits) {
        console.log(`${fruit} weighs ${fruits[fruit]}`)
    } else {
        console.log("unknown fruit! Only: " +Object.keys(fruits))
    }
}
printWeight('dog')
printWeight('banana')