const charIndex = { a : 0, b : 1, c : 2, d : 3, e : 4, 
    f:5,g:6,h:7,i:8,j:9,k:10,l:11,m:12,n:13,o:14,p:15,
    q:16,r:17,s:18,t:19,u:20,v:21,w:22,x:23,y:24,z:25 };


function getCountOfLetters(input) {
    input = input.split(" ").join("");//poistan välilyönnit stringistä
    let counts = [];
    let str = [];
    for (const char in charIndex) {
        counts[char] = 0
        //alustetaan tyhjä array jossa on jokainen kirjain
    }
    
    for (letter of input) {
        counts[letter]++
        /*
        katsotaan jokainen input kirjain ja plussataan se kohta
        counts arrayssa
        */
    }
    console.log(counts)
    for (let letter in counts) {
        str.push(counts[letter])
    }
    console.log(str.join(""))

}

getCountOfLetters("a brown fox and a gray cat")