//JS sorting algorithms insertion sort

const array = [ 4, 19, 7, 1, 9, 22, 6, 13 ];
function sortNumberArray(arr) {
    for (let i=1;i<arr.length;i++) {
        let current = arr[i]
        let j = i -1;
        while (j >= 0 && arr[j] > current) {
            arr[j+1] = arr[j];
            j--;
        }
        arr[j+1] = current

    }
    return arr;

}
const sorted = sortNumberArray(array)
console.log(sorted)