const sentence = "this is a short sentence";
function reverseWords(p){
    const inParts = p.split(" ")
    const reversed = inParts.map(e => e.split("").reverse().join(""))
    return reversed.join(" ")
}
console.log(reverseWords(sentence))