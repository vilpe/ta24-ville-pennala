import('jokes.js');

const text = document.getElementById('text')
let jokesList = document.getElementById('jokesList')

const b1 = document.getElementById('b1')
const b2 = document.getElementById('b2')
const b3 = document.getElementById('b3')
const b4 = document.getElementById('b4')

b1.onclick = e => {
    text.innerHTML = jokes[Math.floor(Math.random() * jokes.length)].joke
}
b2.onclick = e => {
    while (true) {
        joke = jokes[Math.floor(Math.random() * jokes.length)]
        if (joke.categories[0] === 'nerdy') {
            text.innerHTML = joke.joke
            break;
        }
    }
}
b3.onclick = e => {
    for (const joke of jokes) {
        const pTag = document.createElement("p")
        pTag.innerHTML = joke.joke
        jokesList.appendChild(pTag)
    }
}
b4.onclick = e => {
    console.log(jokes.shift());
}

text.innerHTML = jokes[0].joke