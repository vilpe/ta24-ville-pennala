import './App.css';
import { Container, Form, Stack } from 'react-bootstrap';
import Button from 'react-bootstrap/Button';
import 'bootstrap/dist/css/bootstrap.min.css';
//ei toimi ilman tätä??


function App() {

  return (
    <Container className='d-flex justify-content-center'>
      <Form>
        <div className='mb-3'>
          <label className='form-label'>Email address</label>
          <input className='form-control'></input>
        </div>
        <div className='mb-3'>
          <label className='form-label'>Password</label>
          <input className='form-control'></input>
        </div>
        <Stack gap={4}>
          <Button variant='primary'>Submit</Button>
          <Button>Reset form</Button>
        </Stack>
      </Form>
    </Container>
  );
}

export default App;
