import { useState } from 'react';
import './App.css';
import TodoNote from './TodoNote';
import InputForm from './InputForm';

function App() {
  const [todos, setTodos] = useState([
    { id: 1, text: 'Buy potatoes', complete: false },
    { id: 2, text: 'Make food', complete: false },
    { id: 3, text: 'Exercise', complete: false },
    { id: 4, text: 'Do the dishes', complete: false },
    { id: 5, text: 'Floss the teeth', complete: false },
    { id: 6, text: 'Play videogames', complete: true },
  ]);

  const [searchQuery, setSearchQuery] = useState('');

  const changeComplete = (id: number) => {
    const newTodo = todos.map(todo => {
      return todo.id === id ? { ...todo, complete: !todo.complete } : todo;
    });
    setTodos(newTodo);
  };
  const editTodo = (id: number, text: string): void => {
    const newTodo = todos.map(todo => {
      return todo.id === id ? { ...todo, text: text } : todo;
    });
    setTodos(newTodo);
  };
  const deleteTodo = (id: number) => {
    setTodos(todos.filter(todo => todo.id != id));
  };
  const addNewTodo = (text: string) => {
    event?.preventDefault();
    const id = Math.round(Math.random() * 1000);
    setTodos([...todos, { id: id, text: text, complete: false }]);
  };

  const filteredTodos = todos.filter(todo => todo.text.toLowerCase().includes(searchQuery.toLowerCase()));

  const notes = filteredTodos.map((todo) => {
    return <div className='todo' key={todo.id}><TodoNote
      deleteTodo={deleteTodo}
      editTodo={editTodo}
      changeComplete={changeComplete}
      id={todo.id}
      text={todo.text}
      complete={todo.complete} />
    </div>;
  });


  return (
    <div>
      <input
        className='filter'
        type='text'
        onChange={
          (e) => {
            setSearchQuery(e.target.value);
          }}></input>
      <div className='grid'>
        {notes}
      </div>
      <InputForm addNewTodo={addNewTodo} />
    </div>
  );
}

export default App;
