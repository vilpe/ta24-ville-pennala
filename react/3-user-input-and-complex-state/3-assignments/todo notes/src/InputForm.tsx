import { useState } from "react";
import './InputForm.css';

interface InputProps {
    addNewTodo(text: string): void
}
const InputForm = ({ addNewTodo }: InputProps) => {
    const [newTodo, setNewTodo] = useState('');
    const submit = () => {
        addNewTodo(newTodo);
        setNewTodo('');
    };

    return (
        <form className="form">
            <input
                className="input"
                onChange={e => { setNewTodo(e.target.value); }}
                type="text"
                placeholder="Add a new note"
                value={newTodo}></input>
            <button className="button" onClick={submit}>Add note</button>
        </form>
    );
};

export default InputForm;