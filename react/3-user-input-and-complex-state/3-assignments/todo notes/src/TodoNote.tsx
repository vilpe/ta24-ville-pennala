import { useState } from 'react';
import './TodoNote.css';
interface TodoProp {
    id: number,
    text: string,
    complete: boolean,
    changeComplete(id: number): void,
    editTodo(id: number, text: string): void,
    deleteTodo(id: number): void
}
const TodoNote = (props: TodoProp) => {
    const [editMode, setEditMode] = useState(false);
    const [inputText, setInputText] = useState('');
    const todo = props;
    const checkComplete = todo.complete ? 'complete' : 'notcomplete';
    const saveInput = () => {
        todo.editTodo(todo.id, inputText);
    };
    return (
        <div className={checkComplete}>
            <div className='id'>{todo.id}</div>

            {editMode
                ? <div>
                    <input placeholder={todo.text} type='text' onChange={e => { setInputText(e.target.value); }}></input>
                    <button onClick={() => { saveInput(); setEditMode(!editMode); }}>Save</button>
                </div>
                : <div>
                    <p>{todo.text}</p>
                    <button onClick={() => { setEditMode(!editMode); }}>Edit</button>
                </div>
            }

            <input type="checkbox" checked={props.complete} onChange={() => todo.changeComplete(todo.id)}></input>
            {todo.complete.toString()}<br></br>
            <button onClick={() => { todo.deleteTodo(todo.id); }}>X</button>
        </div>
    );
};

export default TodoNote;