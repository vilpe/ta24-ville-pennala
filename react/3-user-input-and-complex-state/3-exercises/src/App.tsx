import Input from './Input';

import './App.css';
import Form from './Form';
import ObjectState from './ObjectState';
import CounterButtonsArray from './CounterButtonsArray';
import ObjectsArray from './ObjectsArray';
import Counter from './Counter';
import { useState } from 'react';
import { v4 as uuidv4 } from 'uuid';

function App() {
  interface CounterModel {
    id: number,
    count: number
  }
  const [counters, setCounters] = useState<CounterModel[]>([
    { id: 1, count: 0 },
    { id: 2, count: 0 },
    { id: 3, count: 0 },
    { id: 4, count: 0 }
  ]);
  const [minimumCounter, setMinimumCounter] = useState(0);
  const [showFilter, setShowFilter] = useState(true);

  const incrementCounter = (id: number) => {
    const newCounters = counters.map(counter => {
      return counter.id === id ? { ...counter, count: counter.count + 1 } : counter;
    });
    setCounters(newCounters);
  };
  const addNewCounter = () => {
    //ääh uuid ei oo number vaan string joten tää laittaa vaan NaN
    //mut toimii
    const id: number = Number(uuidv4());
    setCounters([...counters, { id: id, count: 0 }]);
  };
  const deleteCounter = (id: number) => {
    setCounters(counters.filter(counter => counter.id != id));
  };

  const buttons = counters.map((counter) => {
    //wrappaan diviin ettei vingu keystä
    return <div key={counter.id}><Counter delete={() => deleteCounter(counter.id)} click={() => incrementCounter(counter.id)} id={counter.id} count={counter.count} /></div>;
  });
  const countersToShow = counters.filter(counter => counter.count >= minimumCounter);

  return (
    <div>
      <h2>Input </h2>
      <Input />
      <h2>Form</h2>
      <Form />
      <h2>Object State</h2>
      <ObjectState />
      <h2>Buttons Array</h2>
      <CounterButtonsArray />
      <h2>Objects Array</h2>
      <ObjectsArray />
      <h2>Counters</h2>
      {buttons}
      <button onClick={addNewCounter}>Add new counter</button>
      <h2>Filter counters</h2>
      <input type='checkbox' checked={showFilter} onClick={() => { setShowFilter(!showFilter); }}></input>
      {showFilter &&
        <div>
          <input onChange={e => setMinimumCounter(Number(e.target.value))}></input>
          {countersToShow.map(counter => <p>{counter.count}</p>)}
        </div>}
    </div>
  );
}

export default App;
