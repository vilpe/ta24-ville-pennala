import { useState } from "react";

const CounterButtonsArray = () => {
    const [numbers, setNumbers] = useState([0, 0, 0, 0]);
    const counter = (index: number) => {
        const newNumbers = numbers.map((c, i) => {
            if (i === index) {
                return c + 1;
            } else {
                return c;
            }
        });
        setNumbers(newNumbers);
    };
    const buttons = numbers.map((_v, i: number) => {
        return <button key={i} onClick={() => counter(i)}>{numbers[i]}</button>;
    });
    const sum = numbers.reduce((sum, current) => {
        return sum + current;
    }, 0);
    return (
        <div>
            {buttons}
            Sum:{sum}
        </div >
    );
};

export default CounterButtonsArray;