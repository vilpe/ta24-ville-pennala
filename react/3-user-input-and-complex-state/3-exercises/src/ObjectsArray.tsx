import { useState } from "react";

const ObjectsArray = () => {
    const [counters, setCounters] = useState([
        { id: 1, count: 0 },
        { id: 2, count: 0 },
        { id: 3, count: 0 },
        { id: 4, count: 0 }
    ]);
    const buttons = counters.map(counter => {
        return <button key={counter.id}>{counter.count}</button>;
    });

    return (
        <div>
            {buttons}
        </div>
    );
};

export default ObjectsArray;