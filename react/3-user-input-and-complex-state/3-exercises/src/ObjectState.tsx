import { useState } from "react";

const ObjectState = () => {
    const [inputs, setInputs] = useState({
        number: 0,
        text: ''
    });
    const [display, setDisplay] = useState('');
    const changeText = (event: React.ChangeEvent<HTMLInputElement>) => {
        const newText = { ...inputs, text: event.target.value };
        setInputs(newText);
    };
    const increment = () => {
        const newCount = { ...inputs, number: inputs.number + 1, text: '' };
        setInputs(newCount);
    };
    const changeDisplay = () => {
        setDisplay(inputs.text);
        const emptyText = { ...inputs, text: '' };
        setInputs(emptyText);
    };

    return (
        <div>
            {inputs.number}
            <button onClick={increment}>Increment</button>
            <div>
                <div>
                    Input:{display}
                </div>
                <input onChange={changeText} value={inputs.text}>
                </input>
                <button onClick={changeDisplay}>Submit text</button>

            </div>
        </div >
    );
};
export default ObjectState;

