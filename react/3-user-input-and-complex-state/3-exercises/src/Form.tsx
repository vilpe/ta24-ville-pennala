import { useState } from "react";

const Form = () => {
    const [input, setInput] = useState('');
    const [display, setDisplay] = useState('');
    const handleChange = (event: React.ChangeEvent<HTMLTextAreaElement>) => {
        setInput(event.target.value);
    };
    const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        setDisplay(input);
        setInput('');
    };

    return (
        <div>
            <div>
                Form input:{display}
            </div>
            <form onSubmit={handleSubmit}>
                <textarea value={input} onChange={handleChange} />
                <button type="submit">Submit text</button>
            </form>
        </div >
    );
};

export default Form;