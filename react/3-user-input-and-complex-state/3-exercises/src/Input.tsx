import { useState } from "react";

const Input = () => {
    const [name, setName] = useState('');
    const [display, setDisplay] = useState('');
    const changeText = (event: React.ChangeEvent<HTMLInputElement>) => {
        setName(event.target.value);
    };

    return (
        <div>
            <div>
                Input:{display}
            </div>
            <input onChange={changeText} value={name}>
            </input>
            <button onClick={() => { setDisplay(name); setName(''); }}>Submit text</button>
        </div >
    );
};

export default Input;