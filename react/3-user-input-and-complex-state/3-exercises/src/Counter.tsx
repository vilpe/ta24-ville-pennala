import './Counter.css';
interface CounterProp {
    id: number,
    count: number,
    click(id: number): void,
    delete(id: number): void
}
const Counter = (props: CounterProp) => {
    const { id, count, click } = props;

    return (
        <div className='counter'>
            <button key={id} onClick={() => click(id)}>{count}</button>
            <button onClick={() => props.delete(id)}>Delete</button>
        </div>
    );
};

export default Counter;