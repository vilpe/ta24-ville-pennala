import { useEffect, useState } from 'react';
import axios from 'axios';
import './App.css';

function App() {
  interface NotesFace {
    id: number,
    text: string,
    complete: boolean
  }
  const [notes, setNotes] = useState<NotesFace[]>([]);
  useEffect(() => {
    axios.get('http://localhost:3000/notes')
      .then(res => {
        return setNotes(res.data);
      });
  }, []);

  const handlePost = (event: React.FormEvent<HTMLFormElement>) => {
    const formInputs = new FormData(event.currentTarget);
    axios.post('http://localhost:3000/notes', {
      id: Number(formInputs.get('id')),
      text: formInputs.get('text'),
      complete: Boolean(formInputs.get('complete'))
    });
  };
  const handlePut = (event: React.FormEvent<HTMLFormElement>) => {
    const formInputs = new FormData(event.currentTarget);
    axios.put(`http://localhost:3000/notes/${formInputs.get('id')}`, {
      text: formInputs.get('text'),
      complete: Boolean(formInputs.get('complete'))
    });
  };
  const handleDelete = (event: React.FormEvent<HTMLFormElement>) => {
    const formInputs = new FormData(event.currentTarget);
    axios.delete(`http://localhost:3000/notes/${formInputs.get('id')}`);
  };

  return (
    <div>
      <ul>
        {notes.map(note => {
          return <li key={note.id}>{note.id} {note.text} {note.complete.toString()}</li>;
        })}
      </ul>
      <form onSubmit={handleDelete}>
        Delete form
        <label>
          id
          <input name='id'></input>
        </label>
        <button type='submit'>Delete</button>
      </form>
      <form onSubmit={handlePut}>
        Update form
        <label>
          id
          <input name='id'></input>
        </label>
        <label>
          text
          <input name='text'></input>
        </label>
        <label>
          complete
          <input name='complete'></input>
        </label>
        <button type='submit'>update</button>
      </form>
      <form onSubmit={handlePost}>
        Posting form
        <label>
          id
          <input name="id"></input>
        </label>
        <label>
          text
          <input name="text"></input>
        </label>
        <label>
          complete
          <input name="complete"></input>
        </label>
        <button type='submit'>Post</button>
      </form>
    </div>
  );
}

export default App;
