import { useEffect, useState } from "react";
import noteService from "./noteService";

const Notes = () => {
    interface NoteInterface {
        id: number,
        content: string,
        date: string,
        important: boolean

    }
    const [notes, setNotes] = useState<NoteInterface[]>([]);
    useEffect(() => {
        noteService.getAll()
            .then(res => setNotes(res.data));
    }, []);
    const toggleImportant = (id: number) => {
        const note = notes.find((note) => note.id === id) as NoteInterface;
        const changedNote = { ...note, important: !note.important };
        noteService.update(id, changedNote)
            .then(res => console.log(res));
    };

    return (
        <div>
            <ul>
                {notes.map(note => {
                    return <li key={note.id}>{note.content} {note.important.toString()}<button onClick={() => toggleImportant(note.id)}>toggle important</button></li>;
                })}
            </ul>
            <button onClick={noteService.create}>Post</button>
        </div>
    );
};

export default Notes;