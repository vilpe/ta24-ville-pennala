import axios from "axios";

interface NoteInterface {
    id: number,
    content: string,
    date: string,
    important: boolean

}
const getAll = () => {
    return axios.get('http://localhost:3000/notes');
};
const create = () => {
    const newNote: NoteInterface = {
        id: 100,
        content: 'tämä on konaa',
        date: '11.09.2001',
        important: true
    };
    axios.post('http://localhost:3000/notes', newNote)
        .then(res => console.log(res.data));
};
const update = (id: number, note: NoteInterface) => {
    return axios.put(`http://localhost:3000/notes/${id}`, note);
};
export default { create, getAll, update };