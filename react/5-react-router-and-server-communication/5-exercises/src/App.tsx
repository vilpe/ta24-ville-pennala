import {
  BrowserRouter as Router,
  Routes, Route, Link
} from "react-router-dom";
import './App.css';
import Home from "./Home";
import About from "./About";
import Links from "./Links";
import Notes from "./Notes";

function App() {


  return (
    <Router>
      <div className="linkdiv">
        <Link className="link" to="/">Home </Link>
        <Link className="link" to="/about">About </Link>
        <Link className="link" to="/links">Links </Link>
        <Link className="link" to="/notes">Notes </Link>
      </div>
      <Routes>
        <Route path="/" element={<Home />}></Route>
        <Route path="/about" element={<About />}></Route>
        <Route path="/links" element={<Links />}></Route>
        <Route path="/notes" element={<Notes />}></Route>
      </Routes>
    </Router>
  );
}

export default App;
