interface Planet {
    name: string,
    climate: string
}
interface planetArray {
    planetList: Planet[]
}

const Planets = ({planetList}: planetArray) => {
    //destrukturoin propsin milloin ei tarvitse props.planetList
    const rows = planetList.map(planet => {
        return <tr key={planet.name}><td>{planet.name}</td><td>{planet.climate}</td></tr>
    })
    return (
        <table>
            <tr>
                <th>Planet Name</th>
                <th>Climate</th>
            </tr>
            {rows}
            
        </table>
    )
}

export default Planets;