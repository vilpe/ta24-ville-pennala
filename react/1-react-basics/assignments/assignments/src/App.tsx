import './App.css'
import Greeting from './Greeting';
import r2d2 from './assets/r2d2.jpg'
import Planets from './Planets';
import 'bootstrap/dist/css/bootstrap.min.css'
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import { useState } from 'react';

function App() {
  const [book, setBook] = useState({name:'',pages:0})

  const handleChange = (event) => {
    const {name,value} = event.target
    console.log(event.target.value)
    setBook({...book,[name]:value})
    //taika spread
    console.log(book)
  }
  const handleSubmit = (event) => {
    event.preventDefault();
    list.push({name:book.name,pages:Number(book.pages)})
    console.log(list)
  }

  interface  Book {
    name:string,
    pages:number
  }
  const list:Book[] = [
    {name:"Dune",pages:412},
    {name:"The Eye",pages:782}
  ]
  const listed = list.map(book => {
    return <p>{book.name} ({book.pages})</p>
  })

  const fullName = "ville";
  const age = 20;

  const planetList = [
    { name: "Hoth", climate: "Ice" },
    { name: "Tattooine", climate: "Desert" },
    { name: "Alderaan", climate: "Temperate" },
    { name: "Mustafar", climate: "Volcanic", }
  ];
  

  return (
    <div>
      <h1>Assignment 1</h1>
      <Greeting fullName={fullName} age={age} />

      <h1>Assignment 2</h1>
      <div className="asg2">
        <img src={r2d2}></img>
        <h2>Hello, I am R2D2!</h2>
        <p><i>BeeYoop BeeDeepBoom Weeop DEEpaEEya</i></p>
      </div>

      <h1>Assignment 3</h1>
      <Planets planetList={planetList} />

      <h1 className='mt-3'>Assignment 4</h1>
      {listed}
      <Form onSubmit={handleSubmit}>
        <h3>Add New book</h3>
        <Form.Group controlId='formName'>
          <Form.Control type='text' name='name' placeholder="Book Name" onChange={handleChange}/>
        </Form.Group>
        <Form.Group controlId='formPages'>
        <Form.Control type='number' name='pages' placeholder="Pages Count" onChange={handleChange}/>
        </Form.Group>
        <Button className='mb-5' variant="primary" type="submit">
          Add Book
        </Button>
      </Form>

    </div>
  )
}

export default App
