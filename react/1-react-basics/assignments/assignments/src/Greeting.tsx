interface greetingProps {
    fullName:string,
    age:number
}
const Greeting = (props:greetingProps) => {
    return (
        <div>
            <p>My name is {props.fullName} and I am {props.age} years old!</p>
        </div>
    )
}

export default Greeting;