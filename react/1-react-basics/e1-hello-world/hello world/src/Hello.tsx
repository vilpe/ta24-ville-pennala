import Year from './Year';

interface HelloWorldProps {
    nimi: string,
    year: number
  }
  const Hello = (props: HelloWorldProps) => {
    const name = props.nimi
    const now = new Date();
    const a = 10;
    const b = 20;
  
    return (
      <>
        <div>
          <h1>Hello {name}</h1>
          <p>time is {now.toString()}</p>
          <p>{a} + {b} = {a + b}</p>
          <Year year={props.year} />
        </div>
      </>
    )
  }

export default Hello;