const List = () => {
    const namelist = ["Ari", "Jari", "Kari", "Sari", "Mari", "Sakari", "Jouko"];
    const namesMapped = namelist.map((name, index) => {
        if(index%2===0) {
            return <b key={name}>{name}</b>
        } else {
            return <p key={name}><i>{name}</i></p>
        }
    })
    return (
        <div>
            {namesMapped}
        </div>
    )
}
export default List;