import Hello from './Hello';
import List from './List';

function App() {
  const year = new Date().getFullYear();
  return (
    <div>
      <Hello nimi="ville" year={year} />
      <Hello nimi="kalle" year={year} />
      <Hello nimi="p" year={year} />
      <List />
    </div>
  )
}

export default App
