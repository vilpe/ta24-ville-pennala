import './Person.css';

interface Person {
    name: string,
    age: number
}
const Person = (props: Person) => {
    return (
        <p className='person'>{props.name} is {props.age} years old</p>
    );

};

export default Person;