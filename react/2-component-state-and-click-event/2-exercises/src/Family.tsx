import Person from "./Person";
import './Family.css';

interface OnePerson {
    name: string,
    age: number
}

interface FamilyProps {
    people: OnePerson[]
}
const Family = ({ people }: FamilyProps) => {
    const listOfPeopleComponents = people.map(person => {
        return <Person key={person.name} name={person.name} age={person.age} />;
    });
    return (
        <div className="familyDiv">
            <h3>Family</h3>
            {listOfPeopleComponents}
        </div>
    );
};

export default Family;