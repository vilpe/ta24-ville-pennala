import './App.css';
import Family from './Family';
import Counter from './Counter';
import { useState } from 'react';

const people = [
  { name: 'Alice', age: 30 },
  { name: 'Bob', age: 35 },
  { name: 'Charlie', age: 40 },
  { name: 'Donna', age: 45 }
];


function App() {
  const [sum, setSum] = useState(0);
  const incrementSum = (value: number): void => {
    setSum(sum + value);
  };
  const [showButtons, setShowButtons] = useState(true);
  const [buttonCSS, setButtonCSS] = useState('show');
  const toggleButtons = () => showButtons ? setShowButtons(false) : setShowButtons(true);

  return (
    <div className='app'>
      <p>hello world</p>
      <Family people={people} />
      <button onClick={() => {
        //toggleButtons();
        buttonCSS === 'show' ? setButtonCSS('hide') : setButtonCSS('show');
      }}>
        Show/Hide buttons
      </button>
      {showButtons &&
        <div className={buttonCSS}>
          <Counter incrementSum={incrementSum} />
          <Counter incrementSum={incrementSum} />
          <Counter incrementSum={incrementSum} />
        </div>
      }

      <div>
        Sum of counters: {sum}
      </div>
    </div>
  );
}

export default App;
