import { useState } from "react";

interface CounterProp {
    incrementSum(value: number): void
}

const Counter = ({ incrementSum }: CounterProp) => {
    const [count1, setCount1] = useState(0);

    return (
        <div>
            <button onClick={() => {
                setCount1(count1 + 1);
                incrementSum(1);
            }}>
                Increment
            </button>
            counter: {count1}
        </div>
    );
};

export default Counter;