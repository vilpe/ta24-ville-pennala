import './Bingo.css';
import { useState } from 'react';

const names = [
    "Anakin Skywalker",
    "Leia Organa",
    "Han Solo",
    "C-3PO",
    "R2-D2",
    "Darth Vader",
    "Obi-Wan Kenobi",
    "Yoda",
    "Palpatine",
    "Boba Fett",
    "Lando Calrissian",
    "Jabba the Hutt",
    "Mace Windu",
    "Padmé Amidala",
    "Count Dooku",
    "Qui-Gon Jinn",
    "Aayla Secura",
    "Ahsoka Tano",
    "Ki-Adi-Mundi",
    "Luminara Unduli",
    "Plo Koon",
    "Kit Fisto",
    "Shmi Skywalker",
    "Beru Whitesun",
    "Owen Lars"
];

const five: Array<string> = [];
function checkFive() {
    if (
        five.includes(names[0]) &&
        five.includes(names[1]) &&
        five.includes(names[2]) &&
        five.includes(names[3]) &&
        five.includes(names[4])
    ) {
        console.log('includes');
        window.alert('BINGO');
    }
}

const Bingo = () => {

    const slots = names.map(name => {
        return <Slot name={name} />;
    });

    return (
        <div className="grid">
            {slots}
        </div>
    );
};

interface SlotProps {
    name: string
}
const Slot = (props: SlotProps) => {
    const [clicked, setClicked] = useState(false);
    const bgStyle = {
        backgroundColor: clicked ? 'green' : 'grey'
    };
    return (
        <div id={props.name} key={props.name} style={bgStyle} onClick={(e) => {
            setClicked(!clicked);
            //console.log(e.target.id);
            five.push(e.target.id);
            //console.log(five);
            checkFive();
        }}>{props.name}</div>
    );
};

export default Bingo;