import { useState } from "react";

interface ButtonProp {
    num: number
}

const DisappearingButton = ({ num }: ButtonProp) => {
    const [active, setActive] = useState(true);
    return (
        <div>
            {active &&
                <button onClick={() => { active ? setActive(false) : setActive(true); }}>
                    Button {num}
                </button>
            }
        </div>
    );
};
export default DisappearingButton;