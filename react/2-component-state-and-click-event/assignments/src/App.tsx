import './App.css';
import { useState } from 'react';
import DisappearingButton from './DisappearingButton';
import Bingo from './Bingo';

function App() {
  const [asg1, setAsg1] = useState(true);

  return (
    <div>
      <h1>Assignment 1</h1>
      <div>
        <button onClick={() => asg1 ? setAsg1(false) : setAsg1(true)}>Toggle Text</button>
        {asg1 &&
          <p>Fear is the mind killer</p>
        }
      </div>

      <h1>Assignment 2</h1>
      <DisappearingButton num={1} />
      <DisappearingButton num={2} />
      <DisappearingButton num={3} />
      <DisappearingButton num={4} />
      <DisappearingButton num={5} />

      <h1>Assignment 3</h1>
      <Bingo />
    </div>
  );
}

export default App;
