interface E2Props {
    num1: number,
    num2: number
}

const E2 = (props: E2Props) => {
    const { num1, num2 } = props;
    function over10() {
        return (num1 + num2 >= 10);
    }

    if ((over10())) {
        return (
            <p>prop numbers sum over 10</p>
        );
    } else {
        return (
            <p>prop numbers sum under 10</p>
        );
    }

};

export default E2;