import E2 from './exercise2';

import './App.css';

function App() {
  return (
    <div>
      <p>hello world</p>
      <E2 num1={5} num2={5} />
      <E2 num1={5} num2={2} />
    </div>
  );
}

export default App;
