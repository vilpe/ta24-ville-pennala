import express from 'express';
import dao from './dao.js';

const router = express.Router();
router.use(express.json())

// router.get('/', async (req, res) => {
//     res.send("hellos from router")
// })
router.post('/', async (req, res) => {
    const product = req.body
    const result = await dao.insertProduct(product)
    const storedProduct = { id: result.rows[0], ...product }
    res.send(storedProduct)
})
router.get('/:id', async (req, res) => {
    const id = req.params.id;
    const result = await dao.findOne(id)
    res.send(result.rows)

})
router.put('/:id', async (req, res) => {
    const product = {
        id: req.params.id,
        name: req.body.name,
        price: req.body.price
    };
    await dao.updateProduct(product)
    res.send(product)
})
router.delete('/:id', async (req,res) => {
    const id = req.params.id
    await dao.deleteProduct(id)
    res.status(200).send('deleted')
})
router.get('/', async (req,res) => {
    const result = await dao.findAll();
    res.send(result.rows)
})
export default router;