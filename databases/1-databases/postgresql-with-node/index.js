import express from "express";
import dotenv from 'dotenv';

import { createProductsTable } from "./db.js";
import routes from './router.js';

dotenv.config();

const server = express();

createProductsTable();

server.use('/', routes)

server.listen(process.env.PORT, () => {
    console.log("API listening to port: ", process.env.PORT)
});
