import pg from "pg"
import dotenv from 'dotenv';
dotenv.config();
// console.log(process.env.pg_password)

const pool = new pg.Pool({
    host: process.env.PG_HOST,
    port: Number(process.env.PG_PORT),
    user: process.env.PG_USERNAME,
    password: String(process.env.PG_PASSWORD),
    database: process.env.PG_DATABASE
});

export const executeQuery = async (query, parameters) => {
    const client = await pool.connect();
    const result = await client.query(query, parameters);
    client.release();
    return result;
}

export const createProductsTable = async () => {
    const query = `
        CREATE TABLE IF NOT EXISTS "products" (
            "id" UUID PRIMARY KEY,
            "name" VARCHAR(100) NOT NULL,
            "price" REAL NOT NULL
        )`;
    await executeQuery(query);
    console.log("Products table initialized")
}