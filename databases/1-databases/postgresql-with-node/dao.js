import { executeQuery } from "./db.js";
import { v4 as uuidv4 } from 'uuid';

const insertProduct = async (product) => {
    const id = uuidv4();
    // console.log(product)
    const params = [id, ...Object.values(product)];
    const query = `
        INSERT INTO products ( id, name, price )
        VALUES ( $1, $2, $3 )`;
    const result = await executeQuery(query, params)
    return result;
}
const findOne = async (id) => {
    const query = `
    SELECT * FROM products 
    WHERE id= $1`;
    const result = await executeQuery(query, [id])
    return result;
}
const updateProduct = async (product) => {
    const params = [product.id, product.name, product.price];
    const query = `
    UPDATE products
    SET name = $2, price = $3
    WHERE id = $1`;
    const result = await executeQuery(query, params)
    return result;

}
const deleteProduct = async (id) => {
    const params = [id]
    const query = `
    DELETE FROM products WHERE id = $1`;
    const result = await executeQuery(query, params)
    return result;
}
const findAll = async () => {
    const query = `
    SELECT * FROM products`;
    const result = await executeQuery(query)
    return result;
}
export default { insertProduct, findOne, updateProduct, deleteProduct, findAll };