import express from 'express';

const server = express();

server.get('/', (req, res) => {
    res.send('Ok Get!')
});
server.get('/version', (req, res) => {
    res.send(process.env.VERSION);
});
server.get('/secret', (req, res) => {
    res.send(process.env.SECRET)
});

server.listen(3000, () => {
    console.log('Api running port 3000')
});