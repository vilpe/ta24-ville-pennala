import express from 'express';
import cors from 'cors';
const server = express();
server.use(cors());
server.use(express.json());
//npx tsx server.ts
const notes = [
    { id: 1, text: 'Buy potatoes', complete: false },
    { id: 2, text: 'Make food', complete: false },
    { id: 3, text: 'Exercise', complete: false },
    { id: 4, text: 'Do the dishes', complete: false },
    { id: 5, text: 'Floss the teeth', complete: false },
    { id: 6, text: 'Play videogames', complete: true },
];
//You should import Express the TypeScript way so its types (in @types/express) come along,
//allowing the types of req and res to be inferred from app.get:
server.get('/notes', (_req, res) => {
    res.send(notes);
});
server.post('/notes', (req, res) => {
    notes.push({
        id: req.body.id,
        text: req.body.text,
        complete: req.body.complete
    });
    res.status(201).send(notes);
});
server.put('/notes/:id', (req, res) => {
    const index = notes.findIndex(note => note.id === Number(req.params.id));
    if (req.body.text) {
        notes[index].text = req.body.text;
    }
    if (req.body.complete) {
        notes[index].complete = req.body.complete;
    }
    res.status(204).send('ok');
});
server.delete('/notes/:id', (req, res) => {
    const index = notes.findIndex(note => note.id === Number(req.params.id));
    notes.splice(index, 1);
    res.status(204).send('ok');
});
server.listen(3000, () => { console.log('server running'); });
//# sourceMappingURL=server.js.map